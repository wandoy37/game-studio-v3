<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Laravel\Socialite\Facades\Socialite;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::query()->get();
        return view('admin.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        User::create([
            'name' => $request->name,
            'slug' => Str::slug($request->name, '-'),
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => $request->role
        ]);
        $alert = 'User ' . $request->name . ' has been added!';
        return redirect('admin/user')->with('success', $alert);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($email)
    {
        $user = User::where('email', $email)->first();
        // dd($user);
        return view('admin.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     $user = User::find($id);
    //     $data = [
    //         'name' => $request->name,
    //     ];
    //     // Kondisi jika melakukan upload/edit avatar/foto
    //     if ($request->file('avatar')) {
    //         if ($request->oldAvatar) {
    //             Storage::delete($request->oldAvatar);
    //         }
    //         $data['avatar'] = $request->file('avatar')->store('avatar-image');
    //     }
    //     // Kondisi jika password kosong jangan di update
    //     if ($request->password) {
    //         $data['password'] = Hash::make($request->password);
    //     }
    //     // dd($data);
    //     $user->update($data);
    //     $alert = 'User ' . $request->name . ' has been updated!';
    //     return redirect('admin/dashboard')->with('success', $alert);
    // }

    public function update_vii(Request $request, $id)
    {
        $user = User::find($id);
        $data = [
            'name' => $request->name,
        ];
        // Kondisi jika melakukan upload/edit avatar/foto
        if ($request->file('avatar')) {
            if ($request->oldAvatar) {
                $destinationPath = 'uploads/';
                File::delete($destinationPath . $request->oldAvatar);
            }
            $data['avatar'] = request()->file('avatar')->store('avatars', ['disk' => 'public_uploads']);
        }
        // Kondisi jika password kosong jangan di update
        if ($request->password) {
            $data['password'] = Hash::make($request->password);
        }
        // dd($data);
        $user->update($data);
        $alert = 'User ' . $request->name . ' has been updated!';
        return redirect('admin/dashboard')->with('success', $alert);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if ($user->savatar) {
            Storage::delete($user->savatar);
        }
        $user->delete($user);
        $alert = 'User ' . $user->name . ' has been deleted!';
        return redirect('admin/user')->with('success', $alert);
    }

    // Socialite Google
    public function auth_google()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleProviderCallback()
    {
        $callback = Socialite::driver('google')->stateless()->user();
        $data = [
            'name' => $callback->getName(),
            'slug' => Str::slug($callback->getName(), '-'),
            'email' => $callback->getEmail(),
        ];

        $user = User::firstOrCreate(['email' => $data['email']], $data);
        Auth::login($user, true);

        return redirect('/admin/dashboard');
    }
}