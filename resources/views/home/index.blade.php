@extends('home.layouts.app')

@section('hero')
    @include('home.layouts.components.hero')
@endsection

@section('content')

<!-- Feature section -->
<section class="feature-section spad">
    <div class="container">
        <div class="row">
            @forelse ($latestNews as $posts)
                <div class="col-lg-3">
                    <div class="feature-item set-bg" data-setbg="{{ asset($posts->thumbnail) }}">
                        <span class="cata new">{{ $posts->category->title }}</span>
                        <div class="fi-content text-white">
                            <h5><a href="{{ url("/blog/$posts->slug") }}">{{ $posts->title }}</a></h5>
                            <p>{!! Str::limit($posts->description, 60, '...') !!}</p>
                            <span class="fi-comment">{{ $posts->created_at->format('d, M Y') }}</span>
                        </div>
                    </div>
                </div>
            @empty
            <div class="col-lg p-0">
                <h5>Belum terdapat postingan</h5>
            </div>
            @endforelse
        </div>
        <div class="section-title pt-4">
            <a href="{{ url('/blog') }}" class="text-muted">Tampilkan lebih banyak</a>
        </div>
    </div>
</section>
<!-- Feature section end -->


<!-- Recent game section  -->
<section class="recent-game-section spad set-bg" data-setbg="{{ asset($app->bg_recent_game) }}">
    <div class="container">
        <div class="section-title">
            <div class="cata new">new</div>
            <h2>Recent Games</h2>
        </div>
        <div class="row">
            @forelse ($latestGames as $games)
            <div class="col-lg-4 col-md-6">
                <div class="recent-game-item">
                    <div class="rgi-thumb set-bg" data-setbg="{{ asset($games->thumbnail) }}">
                        <div class="cata new">{{ $games->genre->title }}</div>
                    </div>
                    <div class="rgi-content">
                        <h5><a href="{{ url("/game/$games->slug#details") }}" class="text-dark">{{ $games->title }}</a></h5>
                        <p>{!! Str::limit($games->description, 60, '...') !!}</p>
                        <span class="fi-comment">{{ $games->created_at->format('d, M Y') }}</span>
                    </div>
                </div>
            </div>
            @empty
            <div class="col-lg p-0">
                <h5 class="text-center">Belum terdapat game</h5>
            </div>
            @endforelse
        </div>
        <div class="section-title pt-4">
            <a href="{{ url('/game') }}" class="text-muted">Tampilkan lebih banyak</a>
        </div>
    </div>
</section>
<!-- Recent game section end -->


<!-- Tournaments section -->
{{-- <section class="tournaments-section spad">
    <div class="container">
        <div class="tournament-title">Tournaments</div>
        <div class="row">
            <div class="col-md-6">
                <div class="tournament-item mb-4 mb-lg-0">
                    <div class="ti-notic">Premium Tournament</div>
                    <div class="ti-content">
                        <div class="ti-thumb set-bg" data-setbg="{{ asset('assets/img/tournament/1.jpg') }}"></div>
                        <div class="ti-text">
                            <h4>World Of WarCraft</h4>
                            <ul>
                                <li><span>Tournament Beggins:</span> June 20, 2018</li>
                                <li><span>Tounament Ends:</span> July 01, 2018</li>
                                <li><span>Participants:</span> 10 teams</li>
                                <li><span>Tournament Author:</span> Admin</li>
                            </ul>
                            <p><span>Prizes:</span> 1st place $2000, 2nd place: $1000, 3rd place: $500</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="tournament-item">
                    <div class="ti-notic">Premium Tournament</div>
                    <div class="ti-content">
                        <div class="ti-thumb set-bg" data-setbg="{{ asset('assets/img/tournament/2.jpg') }}"></div>
                        <div class="ti-text">
                            <h4>DOOM</h4>
                            <ul>
                                <li><span>Tournament Beggins:</span> June 20, 2018</li>
                                <li><span>Tounament Ends:</span> July 01, 2018</li>
                                <li><span>Participants:</span> 10 teams</li>
                                <li><span>Tournament Author:</span> Admin</li>
                            </ul>
                            <p><span>Prizes:</span> 1st place $2000, 2nd place: $1000, 3rd place: $500</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<!-- Tournaments section bg -->


<!-- Review section -->
{{-- <section class="review-section spad set-bg" data-setbg="{{ asset('assets/img/review-bg.png') }}">
    <div class="container">
        <div class="section-title">
            <div class="cata new">new</div>
            <h2>Recent Reviews</h2>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="review-item">
                    <div class="review-cover set-bg" data-setbg="{{ asset('assets/img/review/1.jpg') }}">
                        <div class="score yellow">9.3</div>
                    </div>
                    <div class="review-text">
                        <h5>Assasins Creed</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisc ing ipsum dolor sit ame.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="review-item">
                    <div class="review-cover set-bg" data-setbg="{{ asset('assets/img/review/2.jpg') }}">
                        <div class="score purple">9.5</div>
                    </div>
                    <div class="review-text">
                        <h5>Doom</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisc ing ipsum dolor sit ame.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="review-item">
                    <div class="review-cover set-bg" data-setbg="{{ asset('assets/img/review/3.jpg') }}">
                        <div class="score green">9.1</div>
                    </div>
                    <div class="review-text">
                        <h5>Overwatch</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisc ing ipsum dolor sit ame.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="review-item">
                    <div class="review-cover set-bg" data-setbg="{{ asset('assets/img/review/4.jpg') }}">
                        <div class="score pink">9.7</div>
                    </div>
                    <div class="review-text">
                        <h5>GTA</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisc ing ipsum dolor sit ame.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<!-- Review section end -->

@endsection
