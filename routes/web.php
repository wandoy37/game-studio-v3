<?php

use App\Http\Controllers\Auth\SocialiteController as AuthSocialiteController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\ConfigController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GameController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\SocialiteController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\UserController;
use App\Models\Game;
use App\Models\Post;
use Illuminate\Routing\RouteRegistrar;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// HomeController=====================================================================================================
Route::controller(OrderController::class)->group(function () {
    // Home index
    Route::get('/', [HomeController::class, 'home_index'])->name('home.index');

    // Game index
    Route::get('/game', [HomeController::class, 'game_index'])->name('game.index');
    // Game detail
    Route::get('/game/{slug}', [HomeController::class, 'game_detail']);
    // Game berdasarkan genre
    Route::get('/game/genre/{genre:slug}', [HomeController::class, 'game_genre']);

    // Blog index
    Route::get('/blog', [HomeController::class, 'blog_index'])->name('blog.index');
    // Blog single/detail
    Route::get('/blog/{slug}', [HomeController::class, 'blog_detail']);

    // // Blog single/detail comment
    // Route::post('/blog/{slug}', [HomeController::class, 'blog_comment']);
    // // Blog show all comment
    // Route::get('/blog/{slug}/comment', [HomeController::class, 'blog_comment_all']);

    // Blog post berdasarkan category
    Route::get('/blog/category/{category:slug}', [HomeController::class, 'blog_category']);
    // Blog post berdasarkan tag
    Route::get('/blog/tag/{tag:slug}', [HomeController::class, 'blog_tag']);
    // Pages
    Route::get('/about', [HomeController::class, 'about_index']);
    // /Pages
    // Contact
    Route::get('/contact', [HomeController::class, 'contact_index']);
    // Create Messages
    Route::post('/contact/message', [HomeController::class, 'message_create'])->name('create.message');
    // /Contact
});
// end HomeController=====================================================================================================

// CommentController=====================================================================================================
Route::controller(OrderController::class)->group(function () {
    // Store Post Comment
    Route::post('/post/{id}/comment', [CommentController::class, 'post_comment']);
    // Reply post comment
    Route::post('/post/{id}/reply', [CommentController::class, 'post_reply']);

    // Store Game Comment
    Route::post('/game/{id}/comment', [CommentController::class, 'game_comment']);
    // Reply Game comment
    Route::post('/game/{id}/reply', [CommentController::class, 'game_reply']);
});

// Sign-in with google
Route::get('/sign-in-google', [UserController::class, 'auth_google'])->name('auth.google');
Route::get('/auth/google/callback', [UserController::class, 'handleProviderCallback'])->name('auth.google.callback');



Route::prefix('admin')->middleware(['auth'])->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    // Setting management
    Route::get('/setting', [DashboardController::class, 'setting'])->name('setting');
    // Message index
    Route::get('/message', [DashboardController::class, 'message'])->name('message');

    // Configs pages
    Route::get('/config', [DashboardController::class, 'config'])->name('config');
    // Apps pages
    Route::get('/config/app', [ConfigController::class, 'app'])->name('app');
    // Apps update
    Route::patch('/config/app/update', [ConfigController::class, 'app_update'])->name('app.update');

    // Hero Edit
    Route::get('/config/hero/{id}', [ConfigController::class, 'hero_edit']);
    // Hero update
    Route::patch('/config/hero/update/{id}', [ConfigController::class, 'hero_update'])->name('hero.update');

    // Social Media Pages
    Route::get('/config/social-media', [ConfigController::class, 'social_media'])->name('social_media');
    // Social media update
    Route::patch('/config/social-media/update', [ConfigController::class, 'social_media_update'])->name('social.media.update');

    // Contact pages
    Route::get('/config/contact', [ConfigController::class, 'contact'])->name('contact');
    // Contact update
    Route::patch('/config/contact/update', [ConfigController::class, 'contact_update'])->name('contact.update');

    // Confirmasi pesan
    Route::patch('/message/{id}', [MessageController::class, 'update']);

    // Pages management
    Route::get('/pages', [DashboardController::class, 'pages'])->name('pages');
    // Pages edit
    Route::get('/pages/{slug}/edit', [PageController::class, 'edit']);
    // Update pages
    Route::patch('/pages/{id}', [PageController::class, 'update']);
    // End Pages management

    // Filemanager
    Route::group(['prefix' => 'laravel-filemanager',], function () {
        \UniSharp\LaravelFilemanager\Lfm::routes();
    });

    // show all categories
    Route::get('/category', [CategoryController::class, 'index']);
    // store categories
    Route::post('/category', [CategoryController::class, 'store'])->name('category.store');
    // edit categories
    Route::get('/category/{slug}/edit', [CategoryController::class, 'edit']);
    // update categories
    Route::patch('/category/{slug}', [CategoryController::class, 'update']);
    // delete categories
    Route::delete('/category/{slug}', [CategoryController::class, 'destroy']);

    // show all tags
    Route::get('/tag', [TagController::class, 'index']);
    // store tag
    Route::post('/tag', [TagController::class, 'store'])->name('tag.store');
    // edit tag
    Route::get('/tag/{slug}/edit', [TagController::class, 'edit']);
    // update tag
    Route::patch('/tag/{slug}', [TagController::class, 'update']);
    // delete tag
    Route::delete('/tag/{slug}', [TagController::class, 'destroy']);
    // tag select
    Route::get('/tag/select', [TagController::class, 'select'])->name('tags.select');

    // show all posts
    Route::get('/post', [PostController::class, 'index']);
    // create posts
    Route::get('/post/create', [PostController::class, 'create']);
    // store posts
    Route::post('/post', [PostController::class, 'store'])->name('post.store');
    // Show detail post
    Route::get('/post/{slug}', [PostController::class, 'show']);
    // edit posts
    Route::get('/post/{id}/edit', [PostController::class, 'edit'])->name('post.edit');
    // Update post
    Route::patch('/post/{post}', [PostController::class, 'update'])->name('post.update');
    // Delete post
    Route::delete('post/{id}', [PostController::class, 'destroy']);

    // show all genre
    Route::get('/genre', [GenreController::class, 'index'])->name('genre.index');
    // store genre
    Route::post('/genre', [GenreController::class, 'store'])->name('genre.store');
    // update genre
    Route::patch('/genre/{id}', [GenreController::class, 'update']);
    // delete genre
    Route::delete('/genre/{slug}', [GenreController::class, 'destroy']);


    // show all game
    Route::get('/game', [GameController::class, 'index']);
    // create game
    Route::get('/game/create', [GameController::class, 'create']);
    // device select
    Route::get('/device/select', [GameController::class, 'select'])->name('devices.select');
    // store game
    Route::post('/game', [GameController::class, 'store'])->name('game.store');
    // show single game
    Route::get('/game/{slug}', [GameController::class, 'show']);
    // edit game
    Route::get('/game/{slug}/edit', [GameController::class, 'edit']);
    // update game
    Route::patch('/game/{id}', [GameController::class, 'update'])->name('game.update');
    // Delete game
    Route::delete('game/{id}', [GameController::class, 'destroy']);

    // User Profile
    // Edit User Profile
    Route::get('/profile/{email}/edit', [UserController::class, 'edit']);
    // Update User Profile
    Route::patch('/profile/{id}', [UserController::class, 'update_vii']);

    // User management
    Route::get('/user', [UserController::class, 'index']);
    // user create
    Route::get('/user/create', [UserController::class, 'create']);
    // user store
    Route::post('/user', [UserController::class, 'store'])->name('user.store');
    // User edit profile
    Route::get('/user/{email}/edit', [UserController::class, 'edit']);
    // Delete user
    Route::delete('user/{id}', [UserController::class, 'destroy']);
});